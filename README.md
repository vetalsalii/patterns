# Структура репозитория.

## Виды шаблонов проектирования:
- Порождающие (Creational) - абстрагируют процесс инстанцирования. (Как похитрее вызвать конструктор некого объекта.)
- Структурные (Structural) - рассматривают вопрос о том, как из классов и объектов образуются более крупные структуры.
- Поведенчиские (Behavioral) - определяют алгоритмы и способы реализации взаимодействия различных объектов и классов.
- Конкурентные (Concurrency) - используются для более эффективного написания многопоточных программ, и предоставляет готовые решения проблем синхронизации.

## Примеры паттернов в репозитории.

### Creational:
- Abstract factory
- Builder
- Dependency injection
- Factory method
- Lazy initialization
- Multiton
- Object_pool
- Prototype
- Singleton

### Structural:
- Adapter
- Bridge
- Composite
- Decorator
- Dual interface
- Facade
- Flyweight
- Proxy

### Behavioral: Скоро
- Chain of responsibility
- Command
- Interpreter
- Iterator
- Mediator
- Momento
- Observer
- State
- Strategy
- Template method
- Visitor


### Concurrency: Скоро
- Read, write, lock
- Nuclear reaction
- Move
- Guarded suspension
- Double checked locking

