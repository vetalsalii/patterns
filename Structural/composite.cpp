/*
 * Компоновщик (Composite)
 * объединяет объекты в древовидную структуру для представления иерархии
 * от частного к целому. Компоновщик позволяет клиентам обращаться
 * к отдельным объектам и к группам объектов одинаково.
*/

#include <iostream>
#include <vector>
#include <assert.h>

struct FileSystemObject
{
    virtual ~FileSystemObject() = default;

    virtual int size() = 0;
    virtual void add_object(FileSystemObject *) = 0;
};

// У файла есть размер, но нет возможности добавить в него какой либо объект
class File : public FileSystemObject
{
    int size() override
    {
        return 1024;
    }

    void add_object(FileSystemObject *) override
    {
        assert(false);
    }
};

// В классе Directory мы можем получить размер всех файлов
// и добавить в неё файл
class Directory : public FileSystemObject
{
public:
    int size() override
    {
        int total = 0;
        for(auto fo : c)
        {
            total += fo->size();
        }
        return total;
    }

    void add_object(FileSystemObject *fso) override
    {
        c.push_back(fso);
    }
private:
    std::vector<FileSystemObject *> c;
};

Directory* subdir()
{
    Directory *d = new Directory;
    d->add_object(new File);
    d->add_object(new File);
    return d;
}

int main(int, char *[])
{
    Directory* root = new Directory;
    root->add_object(subdir());

    std::cout << root->size() << std::endl;
    // Здесь будет утечка памяти, но данный пример показан для другой ситуации
}
