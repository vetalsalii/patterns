#include <iostream>

// Проблемой является то-что функции члены двух классов
// имеют одинаковые имена

// Интерфейс принтера
class IPrinter
{
public:
    void warmup() { return IPrinter_warmup(); }
    void cooldown() { return IPrinter_cooldown(); }
    virtual ~IPrinter() = default;
private:
    virtual void IPrinter_warmup() = 0;
    virtual void IPrinter_cooldown() = 0;
};

// Интерфейс сканера
class IScanner
{
public:
    void warmup() { return IScanner_warmup(); }
    void cooldown() { return IScanner_cooldown(); }
private:
    virtual void IScanner_warmup() = 0;
    virtual void IScanner_cooldown() = 0;
};

class CPrinter : public IPrinter, public IScanner
{
    void IPrinter_warmup() override
    {
        std::cout << "разогрев принтера\n";
    }

    void IPrinter_cooldown() override
    {
        std::cout << "спящий режим принтера\n";
    }

    void IScanner_warmup() override
    {
        std::cout << "разогрев сканера\n";
    }

    void IScanner_cooldown() override
    {
        std::cout << "спящий режим сканера\n";
    }
};

int main(int, char*[])
{
    auto device = new CPrinter{};
    // device->warmup(); В данном кейсе компилятор не понимает метод какого класса нужно вызывать
    // device->IPrinter::warmup(); Решение проблемы которая находится строкой выше

    IPrinter *printer = device;
    IScanner *scanner = device;

    // Вызывает невиртуальный метод класса IPrinter
    printer->warmup();
    printer->cooldown();

    // Вызывает невиртуальный метод класса IScanner
    scanner->warmup();
    scanner->cooldown();

    delete device;

    return 0;
}
