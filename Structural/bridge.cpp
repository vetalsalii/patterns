/*
 * Мост (Bridge)
 * -------------
 * Структура, позволяющая изменять интерфейс обращения и
 * интерфейс реализации класса независимо.
*/

#include <iostream>

struct ClockSignal
{
    virtual ~ClockSignal() = default;
    virtual void now() = 0;
};

class InternetClockSignal : public ClockSignal
{
    void now() override
    {
        std::cout << "internet clock" << std::endl;
    }
};

class LocalClockSignal : public ClockSignal
{
    void now() override
    {
        std::cout << "local clock" << std::endl;
    }
};

// Мы соединяем две различные иерархии в одном классе,
// через указатель на базовый класс второй иерархии
struct Clock
{
    /* Данный указатель является мостом между двумя иерархиями.
     * Указателей может быть много, источник энергии батарейка, розетка и т.д.
    */
    ClockSignal *s;

    virtual ~Clock() = default;

    void refresh()
    {
        s->now();
        display();
    }
    virtual void display() = 0;
};

class DigitalClock : public Clock
{
    void display()
    {
        std::cout << "digital" << std::endl;
    }
};

class AnalogClock : public Clock
{
    void display()
    {
        std::cout << "analog" << std::endl;
    }
};

int main(int, char *[])
{
    Clock *c = new DigitalClock;
    c->s = new InternetClockSignal;

    c->refresh();

    delete c->s;
    delete c;
}
