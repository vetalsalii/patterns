/*
 * Адаптер (Adapter)
 * -----------------
 * Объект, обеспечивающий взаимодействие двух других объектов,
 * один из которых использует, а другой предоставляет
 * несовместимый с первым интерфейс.
*/

#include <iostream>

struct mysql_client_native
{
    void mysql_connect()
    {
        std::cout << "mysql connect" << std::endl;
    }
    void mysql_execute()
    {
        std::cout << "mysql execute" << std::endl;
    }
    void mysql_close()
    {
        std::cout << "mysql close" << std::endl;
    }
};

struct postgres_client_native
{
    void postgres_open()
    {
        std::cout << "postgres open" << std::endl;
    }
    void postgres_query()
    {
        std::cout << "postgres query" << std::endl;
    }
};

// Интерфейс для работы с базой данных
struct IDatabase
{
    virtual ~IDatabase() = default;

    virtual void connect() = 0;
    virtual void execute_query() = 0;
    virtual void close() = 0;
};

// Так как у нас интерфейсы Postgress и MySql отличаюся,
// пишем для них адаптеры интерфейся
class MysqlDatabase : public IDatabase
{
    // Агрегируем в cебя клиент MySql
    mysql_client_native client;

    // Внутри метода, вызываем методы агрегированного клиента БД
    void connect() override
    {
        client.mysql_connect();
    }
    void execute_query() override
    {
        client.mysql_execute();
    }
    void close() override
    {
        client.mysql_close();
    }
};

class PostgresDatabase : public IDatabase
{
    postgres_client_native client;

    void connect() override
    {
        client.postgres_open();
    }
    void execute_query() override
    {
        client.postgres_query();
    }
    /* Так как у постгреса нет метода close,
     * то переопределяем метод и ничего не делаем
    */
    void close() override
    {
    }
};

int main(int, char *[])
{
    IDatabase *db = new PostgresDatabase{};

    db->connect();
    db->execute_query();
    db->close();

    delete db;
}
