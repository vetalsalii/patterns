/*
 * Приспособленец (Flyweight)
 * Это объект, представляющий себя как уникальный экземпляр в разных
 * местах программы, но фактически не являющийся таковым.
*/

#include <iostream>
#include <map>

// Какае-то картинка
class Image
{
public:
    void resize(int, int) {};
};

// Класс который оптимизирует обработку данных и хранит картинки с различными размерами.
class ImageResizer
{
    std::map<int, Image> images;

public:
    Image get_box(int w)
    {
        auto i = images.find(w);
        // Если у нас картинка с таким размером есть, мы ей возвращаем.
        // в другом случае мы масштабируем её к нужному размеру и возвращаем.
        if (i == images.end())
        {
            Image img;
            img.resize(w, w);

            bool b;
            std::tie(i, b) = images.emplace(w, img);
        }
        return i->second;
    }
};

int main(int, char *[])
{
    ImageResizer rs;

    rs.get_box(100);
    rs.get_box(1000);
    rs.get_box(1000);
}
