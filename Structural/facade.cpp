/*
 * Фасад (Facade)
 * позволяет скрыть сложность системы путём сведения всех возможных внешних вызовов
 * к одному объекту, делегирующему их соответствующим объектам системы.
*/
#include <iostream>

// Система видеонаблюдения
class VideoControl
{
public:
    void info() {};
    void alert() {};
};

// Хранилище транзакций
class Database
{
public:
    void create() {};
    void update() {};
};

// Принтер, что-то печатает
class Printer
{
public:
    void print() {};
};

// Устройство запоминает все деньги взятые у покупателей
class Fiscal
{
public:
    void report() {};
};

// Фасад упрощаем работу с внешним кодом, не несёт никакого архитектурного смысла.
// Просто как предмет украшения.
class Register
{
    VideoControl vc;
    Database db;
    Printer printer;
    Fiscal f;

public:
    void sale_doc()
    {
        db.create();
        vc.info();
        printer.print();
        f.report();
    };

    void ret_doc()
    {
        db.update();
        vc.alert();
        printer.print();
        f.report();
    };
};

int main(int, char *[])
{
    Register r;

    r.sale_doc();
    r.ret_doc();
}
