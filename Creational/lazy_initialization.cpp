/*
 * Ленивая инициализация (Lazy initialization)
 * -------------------------------------------
 * Приём в программировании когда некоторая ресурсоёмкая операция
 * (создание объекта, вычисление значения) выполняется непосредственно перед тем,
 * как будет использован её результат.
 *
*/

#include <iostream>

class File
{
    std::string name;
public:
    explicit File(std::string name)
        :name(std::move(name))
    {
        std::cout << "create " << name << std::endl;
    }

    ~File()
    {
        std::cout << "close " << name << std::endl;
    }

    void write(const std::string& line_)
    {
        std::cout << "write " << line_ << " into " << name << std::endl;
    }
};

class FileOnDemand
{
    std::string name;
    File* file;

public:
    explicit FileOnDemand(std::string name)
        :name(std::move(name)), file(nullptr) { }
    ~FileOnDemand()
    {
        delete file;
    }

    File* operator->()
    {
        if (!file)
        {
            file = new File(name);
        }
        return file;
    }
};

int main(int, char* [])
{

    // Просто запоминаем имя для файла,
    // Для можно сказать делаем отложенный вызов конструктора
    FileOnDemand file("test.txt");

    size_t n = 2/*2*/;
    for (size_t i = 0; i < n; ++i)
    {
        // При первом вызове метода write мы создадим и запишем данные в файл
        file->write(std::to_string(i));
    }

    return 0;
}
