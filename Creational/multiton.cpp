/*
 * Пул одиночек (Multiton)
 * -----------------------
 * порождающий шаблон проектирования, который обобщает шаблон "Одиночка".
 * В то время, как "Одиночка" разрешает создание лишь одного экземпляра класса,
 * мультитон позволяет создавать несколько экземпляров,
 * которые управляются через ассоциативный массив.
*/

#include <iostream>
#include <map>

enum class Tag { main, db };

class Logger {
    Tag t;

public:
    Logger& operator=(const Logger&) = delete;
    static Logger& Instance(Tag t)
    {
        // В данном контейнере будем хранить различные логгеры
        static std::map<Tag, Logger> instance;

        // Пытаемся найти инстанс объекта Tag
        auto i = instance.find(t);
        // Если логгер инстанцирован, то мы просто возвращаем его из функции,
        // в случае если логгер не создан, он будет создан
        if (i == instance.end())
        {
            bool b;
            // Используем tie потому что после метода emplace нам вернётся 2 ссылки, 1 на объект логгера,
            // вторая флаг удачной/неудачной вставки элемента в map
            std::tie(i, b) = instance.emplace(std::make_pair(t, Logger(t)));
        }
        return i->second;
    }

    void info(const std::string& message)
    {
        std::cerr << "   info: [" << int(t) << "] " << message << std::endl;
    }

    void warn(const std::string& message)
    {
        std::cerr << "warning: [" << int(t) << "] " << message << std::endl;
    }

private:
    explicit Logger(Tag t_)
            :t(t_)
    {
    }
};

int main(int, char const**)
{
    Logger::Instance(Tag::main).info("started");
    Logger::Instance(Tag::db).warn("no db");
    Logger::Instance(Tag::main).info("finished");

    return 0;
}
