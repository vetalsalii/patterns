/* Одиночка (Singleton)
 * --------------------
 * Гарантирует, что класс имеет только один экземпляр
 * и предоставляет глобальную точку доступа к нему.
*/

#include <iostream>

class Logger
{
public:
    Logger(const Logger& root) = delete; // [2]
    Logger& operator=(const Logger&) = delete; // [3]

    static Logger& Instance()
    {
        // Инициализация static объекта/переменной является потокобезопасной
        static Logger instance;
        return instance;
    }

    void info(const std::string& message)
    {
        os << "   info: " << message << std::endl;
    }

    void warn(const std::string& message)
    {
        os << "warning: " << message << std::endl;
    }

private:
    Logger() // [1]
        : os{std::cerr} { }

    std::ostream& os; // [3]
};

int main(int, char* [])
{
    // Проблема публичного конструктора, можно создать несколько экземпляров класса, это не будет синглтон
    // Logger sly_newbie; // [1]

    // Проблема в конструкторе копирования, мы можем создать несколько классов вызвав копирующий конструктор
    // Logger sly_adv(Logger::Instance()); // [2]

    // Проблема самоприсваивания
    // Logger::Instance() = Logger::Instance(); // [3]

    Logger::Instance().info("started");
    Logger::Instance().warn("program is empty");

    return 0;
}
