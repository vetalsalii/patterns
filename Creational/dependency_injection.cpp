/*
 * Внедрение зависимости (Dependency Injection)
 * --------------------------------------------
 * Процесс предоставления внешней зависимости программному компоненту.
*/

#include <iostream>

struct IStorage
{
    virtual ~IStorage() = default;

    virtual void execute_query() = 0;
};

// Report Ничего не знает о экземплярах классов с которыми он может работать
class Report
{
    IStorage *storage;

public:
    explicit Report(IStorage *storage_)
        : storage(storage_) {}

    void print()
    {
        storage->execute_query();
        std::cout << "done" << std::endl;
    }
};

class TestStorage : public IStorage
{
    void execute_query() override
    {
        std::cout << "... fetching data" << std::endl;
    }
};


int main(int, char *[])
{
    IStorage *s = new TestStorage;
    // В данном месте происходит иньекция зависимости через конструктор
    // TestStorage в класс Report
    Report report(s);
    report.print();

    delete s;

    return 0;
}
