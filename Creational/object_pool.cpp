/*
 * Объектный пул (Object pool) — порождающий шаблон проектирования,
 * набор инициализированных и готовых к использованию объектов.
 * Когда системе требуется объект, он не создаётся, а берётся из пула.
 * Когда объект больше не нужен, он не уничтожается, а возвращается в пул.
*/

#include <iostream>
#include <vector>

class PgConnection { };

class PgConnectionPool
{
public:
    ~PgConnectionPool()
    {
        // При выходе с области видимости, закрываем все соединения.
        for (const auto& i : pool)
        {
            std::cout << i.connection << std::endl;
            delete i.connection;
        }
    }
    // В данной функции можно ограничить количество соединений и возвращать nullptr при всех занятых соединениях
    PgConnection* get()
    {
        // Если у нас имеется не используемое соединение, мы вернём его и пометим как занят
        for (auto& object : pool)
        {
            if (!object.busy)
            {
                object.busy = true;
                return object.connection;
            }
        }

        // Мы создаём новый экземпляр соединения и помечаем его статус как занят
        auto block = PgConnectionBlock {new PgConnection, true};
        pool.push_back(block);

        return block.connection;
    }

    void put(PgConnection* connection)
    {
        // Освобождаем ранее занятое соединение с бд
        for (auto& object : pool)
        {
            if (object.connection == connection)
            {
                object.busy = false;
                break;
            }
        }
    }

private:
    struct PgConnectionBlock
    {
        PgConnection* connection;
        bool busy;
    };

    std::vector<PgConnectionBlock> pool;
};

int main(int, char* [])
{
    PgConnectionPool pool;


    auto report_conn = pool.get();
    pool.put(report_conn);

    auto admin_conn = pool.get();
    pool.put(admin_conn);

    return 0;
}
