/*
 * Строитель (Builder)
 * ---------
 * Разделяет создание сложного объекта и инициализацию его
 * состояния так, что одинаковый процесс
 * построения может создать объекты с разным состоянием.
*/

#include <cassert>
#include <iostream>
#include <map>
#include <utility>
#include <vector>

class Tariff
{
    const std::vector<double> amount;
    const std::vector<int> discount;

public:
    Tariff(std::vector<double> amount, std::vector<int> discount)
        : amount(std::move(amount))
        , discount(std::move(discount))
    {
        // Проблемой является что автор кода внедрил в свою функцию проверку
        // на согласованность количества данных суммы и скидки для неё.
        assert(this->amount.size() == this->discount.size());
        // amount отсортирован во возрастанию
        // значения в amount уникальны
        // amount[i] соответствует discount[i]
    }

    void apply();
};

class TariffBuilder
{
    std::map<double, int> tariff;

public:
    void add_discount(double subtotal_, int discount_)
    {
        tariff[subtotal_] = discount_;
    }
    // После вызова данного метода, наши данные будут находиться в согласованном
    // состоянии, что не приведёт к падению программы после вызова конструктора класса
    // Tariff
    Tariff build()
    {
        std::vector<double> amount;
        std::vector<int> discount;

        for (const auto& t : tariff)
        {
            amount.push_back(t.first);
            discount.push_back(t.second);
        }

        return Tariff(amount, discount);
    }
};

void Tariff::apply()
{
    std::cout << "tariff is:" << std::endl;
    for (size_t i = 0; i < amount.size(); ++i)
    {
        std::cout << "after "
                  << amount[i] << " UAH apply "
                  << discount[i] << "%"
                  << std::endl;
    }
}

int main(int, char* [])
{
    /*
     * В данной строке имеется проблема, мы не сможем создать класс так как наши данные
     * находятся не в согласованном состоянии, количество сумм не совпадает
     * с количеством данных о скидках.
    */
    Tariff tariff({100, 200, 300}, {10, 15});

    // Решение нашей проблемы мы можем решить и помощью строителя
    auto builder = TariffBuilder();
    builder.add_discount(1000, 10);
    builder.add_discount(20000, 20);
    builder.add_discount(10000, 15);
    auto t = builder.build();

    t.apply();

    return 0;
}
